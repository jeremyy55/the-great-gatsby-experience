import React from 'react';

import Footer from "../components/footer";
import Header from "../components/header";

import '../styles/index.scss';
import layoutStyles from './layout.module.scss';

import Head from './head';

const Layout = (props) => {
    
    return(
        <div className={layoutStyles.container}>
            <Header/>
            <Head title={props.title}/>
            <div className={layoutStyles.content}>
                
                {props.children}
            </div>
            <Footer/>
        </div>
    )
};

export default Layout;