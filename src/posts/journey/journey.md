---
title: "Let's walk for this Incredible Journey !"
date: "2019-10-30" 
---

In few minutes, Audrey and I are leaving for a walk. 

![Fox sometimes need time](./stack.png)

# Where could we go? 
* In a park
* In the street of Mons
* At "Le mont Panisel"
* Where the wind blows
* Further