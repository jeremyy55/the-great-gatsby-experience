import React from "react";
import {Link} from "gatsby";

import Layout from '../components/layout';

const IndexPage = () => {
    return(
        <Layout title="Home">      
            <h1>CV</h1>
            <h2> Hello, I am a civil engineer graduated in computer science and management.</h2>
            <p>Need to contact me ? <a href="/contact">just do it</a></p>
            <p>Need to contact me quickly? <Link to="/contact">Here it is</Link> </p>
        </Layout>
    )
}

export default IndexPage