import React from 'react';
import {Link} from 'gatsby';

import Layout from '../components/layout';

const NotFound = () => {
    return (
        <Layout title="not Found">
            <h1>Page not found! </h1>
            <p> <Link to="/"> Go back Home, son</Link></p>
        </Layout>
    )
}

export default NotFound;