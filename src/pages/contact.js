import React from 'react'

import Layout from '../components/layout';



const ContactPage = () => {
    return(
        <Layout title="Contact">
            <h1>Contact</h1>
            <p>Best way to contact me is on <a href= "https://www.linkedin.com/in/j%C3%A9r%C3%A9my-dumont-36485b197/" target="_blank">linkedIn</a> </p>
        </Layout>

    )
}

export default ContactPage