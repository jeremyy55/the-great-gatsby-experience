const path = require('path');

module.exports.onCreateNode = ({ node, actions }) => {
    const { createNodeField } = actions

    if(node.internal.type ==='MarkdownRemark'){
        const slug = path.basename(node.fileAbsolutePath,'.md')
        createNodeField({
            node,
            name:'slug',
            value:slug,
        })
        }
  }

module.exports.createPages = async ({graphql,actions}) => {
    const {createPage} = actions;
    const blogTemplateForMd = path.resolve('./src/templates/blogFromMd.js');
    const blogTemplateForContentful = path.resolve('./src/templates/blogFromContentful.js');

    const res = await graphql(`
    query{
        allContentfulBlogPost{
            edges{
                node{
                    slug
                }
            }
        }
        allMarkdownRemark{
            edges{
                node{
                    fields{
                        slug
                        }
                    }
                }
            }
        }  
    `) 

    // create blog post from md file here : 
    // as the type of edges is object, i couldn't use forEach directly.
    Array.prototype.forEach.call(res.data.allMarkdownRemark.edges, edge =>createPage({
        component:blogTemplateForMd,
        path:`/blog/${edge.node.fields.slug}`,
        context:{
            slug: edge.node.fields.slug
        }
    }) );

    //and the same from contentful
    Array.prototype.forEach.call(res.data.allContentfulBlogPost.edges,edge => createPage({
        component:blogTemplateForContentful,
        path:`/blog/${edge.node.slug}`,
        context:{
            slug:edge.node.slug
        }
    }))



}